from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import CanvasGrid, ChartModule
from mesa.visualization.UserParam import UserSettableParameter

from dispersion.agents import People, Firm
from dispersion.model import City


def dispersion_portrayal(agent):
    if agent is None:
        return

    portrayal = {}

    if type(agent) is People:
        portrayal["Shape"] = "dispersion/resources/greenPeople.png"
        # https://icons8.com/web-app/433/sheep
        portrayal["scale"] = 0.9
        portrayal["Layer"] = 1

    elif type(agent) is Firm:
        portrayal["Shape"] = "dispersion/resources/firm.png"
        # https://icons8.com/web-app/36821/German-Shepherd
        portrayal["scale"] = 0.9
        portrayal["Layer"] = 2
#        portrayal["text"] = round(agent.Wage, 1)
        portrayal["text_color"] = "White"

#    elif type(agent) is GrassPatch:
#        if agent.fully_grown:
#            portrayal["Color"] = "#00AA00"
#        else:
#            portrayal["Color"] = "#D6F5D6"
#        portrayal["Shape"] = "rect"
#        portrayal["Filled"] = "true"
#        portrayal["Layer"] = 0
#        portrayal["w"] = 1
#        portrayal["h"] = 1

    return portrayal


canvas_element = CanvasGrid(dispersion_portrayal, 100, 100, 1000, 1000)

chart_people_num = ChartModule([{"Label": "A城人数", "Color": "#AA0000"},
                             {"Label": "B城人数", "Color": "#666666"}])
chart_citizen_distribution = ChartModule([{"Label": "工厂数", "Color": "#AA0000"},
                             {"Label": "人数", "Color": "#666666"}])
chart_firm_efficiency = ChartModule([{"Label": "Wolves", "Color": "#AA0000"},
                             {"Label": "Sheep", "Color": "#666666"}])
chart_firm_num = ChartModule([{"Label": "A城工厂数", "Color": "#AA0000"},
                             {"Label": "B城工厂数", "Color": "#666666"}])
chart_firm_profit = ChartModule([{"Label": "A城工厂利润", "Color": "#AA0000"},
                             {"Label": "B城工厂利润", "Color": "#666666"}])
chart_aver_mar_cost = ChartModule([{"Label": "A城工厂平均边际成本", "Color": "#AA0000"},
                             {"Label": "B城工厂平均边际成本", "Color": "#666666"}])
chart_aver_fixed_cost = ChartModule([{"Label": "A城工厂平均固定成本", "Color": "#AA0000"},
                             {"Label": "B城工厂平均固定成本", "Color": "#666666"}])
chart_citizen_aver_income = ChartModule([{"Label": "A城居民平均收入", "Color": "#AA0000"},
                             {"Label": "B城居民平均收入", "Color": "#666666"}])
chart_aver_util = ChartModule([{"Label": "A城居民平均效用", "Color": "#AA0000"},
                             {"Label": "B城居民平均效用", "Color": "#666666"}])

#model_params = {"grass": UserSettableParameter('checkbox', 'Grass Enabled', False),
#                "grass_regrowth_time": UserSettableParameter('slider', 'Grass Regrowth Time', 20, 1, 50),
#                "initial_sheep": UserSettableParameter('slider', 'Initial Sheep Population', 100, 10, 300),
#                "sheep_reproduce": UserSettableParameter('slider', 'Sheep Reproduction Rate', 0.04, 0.01, 1.0,
#                                                         0.01),
#                "initial_wolves": UserSettableParameter('slider', 'Initial Wolf Population', 50, 10, 300),
#                "wolf_reproduce": UserSettableParameter('slider', 'Wolf Reproduction Rate', 0.05, 0.01, 1.0,
#                                                        0.01,
#                                                        description="The rate at which wolf agents reproduce."),
#                "wolf_gain_from_food": UserSettableParameter('slider', 'Wolf Gain From Food Rate', 20, 1, 50),
#                "sheep_gain_from_food": UserSettableParameter('slider', 'Sheep Gain From Food', 4, 1, 10),
#
model_params = {"initial_a_people": UserSettableParameter('slider', 'numbers of people in a city', 100, 100, 5000),
                "initial_b_people": UserSettableParameter('slider', 'numbers of people in b city', 100, 100, 5000),
                "initial_a_wise": UserSettableParameter('slider', 'a wise', 50, 1, 200),
                "initial_b_wise": UserSettableParameter('slider', 'b wise', 50, 1, 200),
                "initial_a_wise_sd": UserSettableParameter('slider', 'a wise standard error', 5, 1, 100),
                "initial_b_wise_sd": UserSettableParameter('slider', 'b wise standard error', 5, 1, 100),
                "initial_a_firm": UserSettableParameter('slider', 'a firm', 75, 1, 200),
                "initial_b_firm": UserSettableParameter('slider', 'b firm', 75, 1, 200),
                "init_a_margin_cost": UserSettableParameter('slider', 'a margin cost', 1.2, 0.1, 10, 0.1),
                "init_b_margin_cost": UserSettableParameter('slider', 'b margin cost', 1.2, 0.1, 10, 0.1),
                "init_a_fixed_cost": UserSettableParameter('slider', 'a fixed cost', 25, 1, 100),
                "init_b_fixed_cost": UserSettableParameter('slider', 'b fixed cost', 25, 1, 100),
                "init_a_t_cost": UserSettableParameter('slider', 'a t cost', 1.2, 0.1, 10, 0.1),
                "init_b_t_cost": UserSettableParameter('slider', 'b t cost', 1.2, 0.1, 10, 0.1),
                "init_ab_t_cost": UserSettableParameter('slider', 'ab t cost', 5, 0.1, 20, 0.1),
                "init_sigma": UserSettableParameter('slider', 'sigma', 2, 0.1, 5,0.1),
                "init_sigma_a": UserSettableParameter('slider', 'sigma a', 2.5, 1, 10, 0.1),
                "init_sigma_b": UserSettableParameter('slider', 'sigma a', 2.5, 1, 10, 0.1),
                "init_people_move_ratio": UserSettableParameter('slider', 'people move ratio', 0.01, 0.01, 0.1, 0.01),
                "init_firms_move_ratio": UserSettableParameter('slider', 'firms move ratio', 0.05, 0, 0.1, 0.01),
                "init_num_of_birth_firms": UserSettableParameter('slider', 'num of birth firms', 5, 1, 10),
                "init_firms_die_ratio": UserSettableParameter('slider', 'firms die ratio', 0.05, 0, 0.5, 0.01),
                "init_ratio_of_bigger":UserSettableParameter('slider', 'firms die ratio', 0.05, 0, 0.5, 0.01),
                "init_change_firms": UserSettableParameter('checkbox', 'If change firms', True),
                "init_depend_on_fixed_cost": UserSettableParameter('checkbox', 'If change firms', True),
                "init_w_change": UserSettableParameter('checkbox', 'If depend on fixed cost', True),
#                "CES_function": UserSettableParameter('choice', 'CES Function', "two level", "two level", "CP model")
                "CES_function" : UserSettableParameter('choice', 'Utility function', value='Two level',
                                              choices=['Two level', 'CP model'])
                }

server = ModularServer(City, [canvas_element, chart_people_num, chart_citizen_distribution,
                              chart_firm_efficiency, chart_firm_num, chart_firm_profit,
                              chart_aver_mar_cost, chart_aver_fixed_cost, chart_citizen_aver_income,
                              chart_aver_util], "dispersion", model_params)
server.port = 8521
