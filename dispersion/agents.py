import random

from mesa import Agent
#from dispersion.model import position

from dispersion.random_walk import RandomWalker


import numpy as np




class People(RandomWalker):
    '''
    
    '''

    energy = None

    def __init__(self, unique_id,types, pos, model, moore, wage):
        super().__init__(unique_id, pos, model, moore=moore)
        self.Wage = wage
        self.pos = pos
        self.Types = types
#        self.Types = types




    def step(self):
        '''
        A model step. Move, then eat grass and reproduce.
        '''
        pass
#        self.random_move()
#        living = True
#
#        if self.model.grass:
#            # Reduce energy
#            self.Wage -= 1
#
#            # If there is grass available, eat it
#            this_cell = self.model.grid.get_cell_list_contents([self.pos])
#            grass_patch = [obj for obj in this_cell
#                           if isinstance(obj, GrassPatch)][0]
#            if grass_patch.fully_grown:
#                self.Wage += self.model.sheep_gain_from_food
#                grass_patch.fully_grown = False
#
#            # Death
#            if self.Wage < 0:
#                self.model.grid._remove_agent(self.pos, self)
#                self.model.schedule.remove(self)
#                living = False

#        if living and random.random() < self.model.sheep_reproduce:
            # Create a new sheep:
#            if self.model.grass:
#                self.Wage /= 2
#            lamb = People(self.pos, self.model, self.moore, self.Wage)
#            self.model.grid.place_agent(lamb, self.pos)
#            self.model.schedule.add(lamb)

#########################
    def real_wage(self, people_num, population_coefficient):
        '''
        根据名义工资、本地人口数和人口指数计算实际工资
        '''
        self.RealWages= self.Wage * (0.5 + 0.5 * np.tanh(2-people_num/population_coefficient))

    def expect_wage(self, average, stderr):
        '''
        计算异地期望工资
        :param average:期望工资均值
        :param stderr: 期望工资标准偏差
        :return: 期望工资
        '''
        self.ExpectWages = random.normalvariate(average,stderr)

    def expect_real_wage(self, people_num, population_coefficient):
        '''
        根据异地期望工资、异地人口数和人口系数计算异地期望的实际工资
        :param people_num:城市人口数
        :param population_coefficient:城市人口系数
        :param respectwages:异地期望工资
        :return:期望实际工资
        '''
        self.ExpectRealWages = self.ExpectWages * (0.5 + 0.5 * np.tanh(2-people_num/population_coefficient))

    def local_utility(self,types,sigma):
        if self.Types == 'a':
            self.LocalUtility = self.RealWages/(self.model.A_LocalIndex * (1+self.model.A_Index) **(sigma-1))
        elif self.Types == 'b':
            self.LocalUtility = self.RealWages/(self.model.B_LocalIndex * (1+self.model.B_Index) **(sigma-1))

    def nonlocal_utility(self,type,sigma):
        if self.Types == 'a':
            self.NonlocalUtility = self.RealWages/(self.model.A_NonlocalIndex*(1+self.model.A_Index)**(1-sigma))
        elif self.Types == 'b':
            self.NonlocalUtility = self.RealWages/(self.model.B_NonlocalIndex*(1+self.model.B_Index)**(1-sigma))

    def expect_local_utility(self,type,sigma):
        if self.Types == 'a':
            self.ExpectLocalUtility = self.ExpectRealWages/(self.model.B_NonlocalIndex * (1+self.model.A_Index)**(sigma-1))
        elif self.Types == 'b':
            self.ExpectLocalUtility = self.ExpectRealWages/(self.model.A_NonlocalIndex * (1+self.model.B_Index)**(sigma-1))

    def expect_nonlocal_utility(self,type,sigma):
        if self.Types == 'a':
            self.ExpectNonlocalUtility = self.ExpectRealWages/(self.model.A_LocalIndex * (1+self.model.B_Index)**(1-sigma))
        elif self.Types == 'b':
            self.ExpectNonlocalUtility = self.ExpectRealWages/(self.model.B_LocalIndex * (1+self.model.A_Index)**(1-sigma))

    def utility(self,type, sigma):
        if self.Types == 'a':
            self.Utility = self.LocalUtility**((sigma-1)/sigma)+self.NonlocalUtility**((sigma-1)/sigma)**(sigma/(sigma-1))
            self.ExpectUtility = self.ExpectLocalUtility**((sigma-1)/sigma) + self.ExpectNonlocalUtility**((sigma-1)/sigma)**(sigma/(sigma-1))
        elif self.Types == 'b':
            self.Utility = self.LocalUtility**((sigma-1)/sigma)+self.NonlocalUtility**((sigma-1)/sigma)**(sigma/(sigma-1))
            self.ExpectUtility = self.ExpectLocalUtility**((sigma-1)/sigma) + self.ExpectNonlocalUtility**((sigma-1)/sigma)**(sigma/(sigma-1))

    def move(self):
        if (self.ExpectUtility-self.Utility)/self.Utility > self.model.ratio_of_bigger:
            if random.random() < self.model.people_move_ratio:
                self.Wage = self.RealWages
                self.model.grid._remove_agent(self.pos, self)
                self.model.schedule.remove(self)
                if self.Types =='a':
                    self.model.a_people.remove(self)
                    self.Types = 'b'
                    pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'b')
                    self.pos = pos
                    self.model.grid.place_agent(self, pos)
#                    self.model.add(self)
                    self.model.b_people.append(self)
                    self.model.schedule.add(self)
                elif self.Types == 'b':
                    self.model.b_people.remove(self)
                    self.Types = 'a'
                    pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'a')
                    self.pos = pos
                    self.model.grid.place_agent(self, pos)
#                    self.model.add(self)
                    self.model.a_people.append(self)
                    self.model.schedule.add(self)

class Firm(RandomWalker):
    '''
    A wolf that walks around, reproduces (asexually) and eats sheep.
    '''

    energy = None

    def __init__(self,unique_id,types, pos, model, moore, wage, margin, fixed_cost):
        super().__init__(unique_id,pos, model, moore=moore)
        self.Types = types
        self.Wage = wage
        self.Margin = margin
        self.pos = pos
        self.FixedCost = fixed_cost
        self.Profit = 0
        self.ExpectProfit = 0

    def step(self):
        pass
#        self.random_move()
#        self.Wage -= 1

        # If there are sheep present, eat one
#        x, y = self.pos
#        this_cell = self.model.grid.get_cell_list_contents([self.pos])
#        sheep = [obj for obj in this_cell if isinstance(obj, People)]
#        if len(sheep) > 0:
#            sheep_to_eat = random.choice(sheep)
#            self.Wage += self.model.wolf_gain_from_food
#
            # Kill the sheep
#            self.model.grid._remove_agent(self.pos, sheep_to_eat)
#            self.model.schedule.remove(sheep_to_eat)

        # Death or reproduction
#        if self.Wage < 0:
#            self.model.grid._remove_agent(self.pos, self)
#            self.model.schedule.remove(self)
#        else:
#            if random.random() < self.model.wolf_reproduce:
                # Create a new wolf cub
#                self.Wage /= 2
#                cub = Firm(self.pos, self.model, self.moore, self.Wage, self.Margin, self.FixedCost)
#                self.model.grid.place_agent(cub, cub.pos)
#                self.model.schedule.add(cub)


################################################################
    def nominal_price(self, sigma):
        '''
        计算名义价格
        :param sigma:
        :return: 名义价格
        '''
        self.NominalPrice = self.Margin * sigma/(sigma-1)

    def real_price(self, sigma):
        '''
        计算实际价格
        :param sigma:
        :return: 实际价格
        '''
        self.RealPrice = self.NominalPrice**(1-sigma)

    def local_product_num(self, type, cost, sum_wages, sigma):
        if self.Types == 'a':
            return (sum_wages * (cost*self.NominalPrice)**(-sigma))/(self.model.A_LocalIndex**(1-sigma)*(1+self.model.A_Index ** (self.model.sigma-1)))
        elif self.Types == 'b':
            return (sum_wages * (cost*self.NominalPrice)**(-sigma))/(self.model.B_LocalIndex**(1-sigma)*(1+self.model.B_Index ** (self.model.sigma-1)))

    def nonlocal_product_num(self,type, cost,Tcost,sum_wages, sigma):
        if self.Types == 'a':
            return (sum_wages * (cost*Tcost*self.NominalPrice)**(-sigma))/(self.model.B_NonlocalIndex**(1-sigma)*(1+self.model.B_Index ** (self.model.sigma-1)))
        elif self.Types == 'b':
            return (sum_wages * (cost*Tcost*self.NominalPrice)**(-sigma))/(self.model.A_NonlocalIndex**(1-sigma)*(1+self.model.A_Index ** (self.model.sigma-1)))

    def productNumber(self,type, cost1, cost2, Tcost, sum_wages1, sum_wages2, sigma1, sigma2):
        if self.Types == 'a':
            return cost1 * self.local_product_num(type, cost1, sum_wages1,sigma1) + cost2*Tcost*self.nonlocal_product_num(type,cost2, Tcost, sum_wages2, sigma1)
        elif self.Types == 'b':
            return cost2 * self.local_product_num(type, cost2, sum_wages2,sigma2) + cost1*Tcost*self.nonlocal_product_num(type,cost1, Tcost, sum_wages1, sigma2)

    def profit(self, type, cost1,cost2, Tcost,sum_wages1,sum_wages2, sigma1, sigma2):
        if self.Types == 'a':
            number = self.productNumber(type, cost1,cost2,Tcost,sum_wages1,sum_wages2,sigma1,sigma2)
            self.Profit = self.NominalPrice * number-self.FixedCost-self.Margin*number
            return  self.Profit
        elif self.Types == 'b':
            number = self.productNumber(type, cost1,cost2,Tcost,sum_wages1,sum_wages2,sigma1,sigma2)
            self.Profit = self.NominalPrice * number-self.FixedCost-self.Margin*number
            return self.Profit

    def expect_local_product_num(self, type,cost,Tcost,sum_wages, sigma):
        if self.Types == 'a':
            return (sum_wages * (cost * Tcost * self.NominalPrice)**(-sigma))/(self.model.A_NonlocalIndex **(1-sigma)*(1+self.model.A_Index ** (1 - self.model.sigma)))
        elif self.Types == 'b':
            return (sum_wages * (cost * Tcost * self.NominalPrice)**(-sigma))/(self.model.B_NonlocalIndex **(1-sigma)*(1+self.model.B_Index ** (1 - self.model.sigma)))

    def expect_nonlocal_product_num(self, type, cost,Tcost, sum_wages, sigma):
        if self.Types == 'a':
            return (sum_wages * (cost*self.NominalPrice)**(-sigma))/(self.model.B_LocalIndex**(1-sigma)*(1+self.model.B_Index**(self.model.sigma - 1)))
        elif self.Types == 'b':
            return (sum_wages * (cost*self.NominalPrice)**(-sigma))/(self.model.A_LocalIndex**(1-sigma)*(1+self.model.A_Index**(self.model.sigma - 1)))

    def expect_productNumber(self,type,cost1, cost2, Tcost, sum_wages1, sum_wages2, sigma1, sigma2):
        if self.Types == 'a':
            return cost2 * self.expect_nonlocal_product_num(type, cost2, Tcost, sum_wages2, sigma2) + cost1 * Tcost*self.expect_local_product_num(type, cost1, Tcost, sum_wages1,sigma2)
        elif self.Types == 'b':
            return cost1 * self.expect_nonlocal_product_num(type, cost1, Tcost, sum_wages1, sigma1) + cost2 * Tcost*self.expect_local_product_num(type, cost2, Tcost, sum_wages2,sigma1)

    def expect_profit(self,type, cost1,cost2,Tcost,sum_wages1,sum_wages2,sigma1,sigma2):
        if self.Types == 'a':
            number = self.expect_productNumber(type,cost1,cost2,Tcost,sum_wages1,sum_wages2,sigma1,sigma2)
            self.ExpectProfit = self.NominalPrice * number-self.FixedCost-self.Margin*number
            return self.ExpectProfit
        elif self.Types == 'b':
            number = self.expect_productNumber(type,cost1,cost2,Tcost,sum_wages1,sum_wages2,sigma1,sigma2)
            self.ExpectProfit = self.NominalPrice * number-self.FixedCost-self.Margin*number
            return self.ExpectProfit

    def die(self):
        self.model.grid._remove_agent(self.pos, self)
        self.model.schedule.remove(self)
        if self.Types == 'a':
            self.model.a_firm.remove(self)
        elif self.Types == 'b':
            self.model.b_firm.remove(self)

    def build(self,pos, types):
        if types == 'a':
            wage_a = random.normalvariate(self.model.a_wise, self.model.a_wise_sd)
            firm = Firm(random.randint(1,1000),'a',pos, self.model, True, wage_a, self.model.a_margin_cost, self.model.a_fixed_cost)
            self.model.grid.place_agent(firm, pos)
            self.model.schedule.add(firm)
            self.model.a_firm.append(firm)
        elif types == 'b':
            wage_b = random.normalvariate(self.model.b_wise, self.model.b_wise_sd)
            firm = Firm(random.randint(1,1000),'b',pos, self.model, True, wage_b, self.model.b_margin_cost, self.model.b_fixed_cost)
            self.model.grid.place_agent(firm, pos)
            self.model.schedule.add(firm)
            self.model.b_firm.append(firm)


    def move(self):
        if self.Profit != 0 or self.ExpectProfit !=0:
            if self.Profit < 0:
                if random.random() < self.model.firms_die_ratio:
                    self.die()
#            if self.model.A_AverageProfit < self.model.B_AverageProfit:
#                pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'b')
#                self.build(pos, 'b')
#            else:
#                pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'a')
#                self.build(pos, 'a')
            if (self.ExpectProfit - self.Profit)/self.Profit > self.model.ratio_of_bigger:
                if random.random() < (self.model.firms_move_ratio + (self.model.a_fixed_cost-self.FixedCost)/self.model.a_fixed_cost*self.model.firms_move_ratio):
                    if self.model.verbose:
                        print('move')
                    if self.Types =='a':
                        self.Types = 'b'
                        pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'b')
                        self.model.grid._remove_agent(self.pos, self)
                        self.model.grid.place_agent(self, pos)
                        self.pos = pos
                        self.model.a_firm.remove(self)
                        self.model.b_firm.append(self)
                    elif self.Types == 'b':
                        self.Types = 'a'
                        pos = self.model.position(self.model.height,self.model.width, 0.05* self.model.height, 'a')
                        self.model.grid._remove_agent(self.pos, self)
                        self.model.grid.place_agent(self, pos)
                        self.pos = pos
                        self.model.b_firm.remove(self)
                        self.model.a_firm.append(self)


#class GrassPatch(Agent):
#    '''
#    A patch of grass that grows at a fixed rate and it is eaten by sheep
#    '''
#
#    def __init__(self, pos, model, fully_grown, countdown):
#        '''
#        Creates a new patch of grass
#
#        Args:
#            grown: (boolean) Whether the patch of grass is fully grown or not
#            countdown: Time for the patch of grass to be fully grown again
#        '''
#        super().__init__(pos, model)
#        self.fully_grown = fully_grown
#        self.countdown = countdown

#    def step(self):
#        if not self.fully_grown:
#            if self.countdown <= 0:
                # Set as fully grown
#                self.fully_grown = True
#                self.countdown = self.model.grass_regrowth_time
#            else:
#                self.countdown -= 1
