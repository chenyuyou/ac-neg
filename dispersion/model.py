'''

'''

import random

from mesa import Model
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector

from dispersion.agents import People, Firm

from dispersion.schedule import RandomActivationByBreed




class City(Model):
    '''
    Wolf-Sheep Predation Model
    '''





    verbose = True  # Print-monitoring

    description = 'A model for simulating wolf and sheep (predator-prey) ecosystem modelling.'

    def __init__(self, height=100, width=100,
                 initial_a_people=100, initial_b_people=100,
                  initial_a_wise=50, initial_b_wise=50,
                 initial_a_wise_sd=5, initial_b_wise_sd=5,
                 initial_a_firm= 75, initial_b_firm = 75,
                 init_a_margin_cost = 1.2, init_b_margin_cost = 1.2,
                 init_a_fixed_cost = 25, init_b_fixed_cost = 25,
                 init_a_t_cost = 1.2, init_b_t_cost = 1.2,
                 init_ab_t_cost = 5.0, init_sigma = 2.0,
                 init_sigma_a = 2.5, init_sigma_b = 2.5,
                 init_people_move_ratio = 0.01, init_firms_move_ratio = 0.05, init_num_of_birth_firms = 5,
                 init_firms_die_ratio = 0.05, init_ratio_of_bigger = 0.05,
                 init_change_firms = True, init_depend_on_fixed_cost = False, init_w_change = False, CES_function = "two-level"):

#                 grass = False, fixed_cost = False,
#                 initial_sheep=100, initial_wolves=50,
#                 sheep_reproduce=0.04, wolf_reproduce=0.05,
#                 wolf_gain_from_food=20,
#                 grass_regrowth_time=30, sheep_gain_from_food=4):
        '''
        Create a new Wolf-Sheep model with the given parameters.

        Args:
            initial_sheep: Number of sheep to start with
            initial_wolves: Number of wolves to start with
            sheep_reproduce: Probability of each sheep reproducing each step
            wolf_reproduce: Probability of each wolf reproducing each step
            wolf_gain_from_food: Energy a wolf gains from eating a sheep
            grass: Whether to have the sheep eat grass for energy
            grass_regrowth_time: How long it takes for a grass patch to regrow
                                 once it is eaten
            sheep_gain_from_food: Energy sheep gain from grass, if enabled.
        '''
        self.height = height
        self.width = width
        self.a_people_num = initial_a_people
        self.b_people_num = initial_b_people
        self.a_wise = initial_a_wise
        self.b_wise = initial_b_wise
        self.a_wise_sd = initial_a_wise_sd
        self.b_wise_sd = initial_b_wise_sd
        self.a_firm_num = initial_a_firm
        self.b_firm_num = initial_b_firm
        self.a_margin_cost = init_a_margin_cost
        self.b_margin_cost = init_b_margin_cost
        self.a_fixed_cost = init_a_fixed_cost
        self.b_fixed_cost = init_b_fixed_cost
        self.a_t_cost = init_a_t_cost
        self.b_t_cost = init_b_t_cost
        self.ab_t_cost = init_ab_t_cost
        self.sigma = init_sigma
        self.sigma_a = init_sigma_a
        self.sigma_b = init_sigma_b
        self.people_move_ratio = init_people_move_ratio
        self.firms_move_ratio = init_firms_move_ratio
        self.num_of_birth_firms = init_num_of_birth_firms
        self.firms_die_ratio = init_firms_die_ratio
        self.ratio_of_bigger = init_ratio_of_bigger
        self.change_firms = init_change_firms
        self.depend_on_fixed = init_depend_on_fixed_cost
        self.w_change = init_w_change


        self.a_people = []
        self.b_people = []

        self.a_firm = []
        self.b_firm = []

        self.A_Profit = 0
        self.B_Profit = 0

        self.A_AverageProfit = 0
        self.B_AverageProfit = 0

        self.A_AverageUtility = 0
        self.B_AverageUtility = 0
        # Set parameters


        self.schedule = RandomActivationByBreed(self)
        self.grid = MultiGrid(self.height, self.width, torus=True)
        self.datacollector = DataCollector(
            model_reporters={
                "A城人数": lambda m: m.count(self.a_people_num),
                "B城人数": lambda m: m.count(self.b_people_num),
                "工厂数": lambda m: m.schedule.get_breed_count(Firm),
                "人数": lambda m: m.schedule.get_breed_count(People),
                "A城人": lambda m: m.schedule.get_breed_count(Firm),
                "B城人": lambda m: m.schedule.get_breed_count(People),
                "A城工厂数": lambda m: m.count_firm_num('a'),
                "B城工厂数": lambda m: m.count_firm_num('b'),
                "A城工厂利润": lambda m: m.count(self.A_Profit),
                "B城工厂利润": lambda m: m.count(self.B_Profit),
                "A城工厂平均边际成本": lambda m: m.count(self.a_margin_cost/self.a_firm_num),
                "B城工厂平均边际成本": lambda m: m.count(self.b_margin_cost/self.b_firm_num),
                "A城工厂平均固定成本": lambda m: m.count(self.a_fixed_cost/self.a_firm_num),
                "B城工厂平均固定成本": lambda m: m.count(self.b_fixed_cost/self.b_firm_num),
                "A城居民平均收入": lambda m: m.count(self.A_AverageProfit),
                "B城居民平均收入": lambda m: m.count(self.B_AverageProfit),
                "A城居民平均效用": lambda m: m.count(self.A_AverageUtility),
                "B城居民平均效用": lambda m: m.count(self.B_AverageUtility),
             },
#            agent_reporters={"Energy":lambda a: getattr(a, 'energy', None),
#            }
            )

        # Create a city people:
        for i in range(initial_a_people):
            x, y = self.position(self.height,self.width, 0.05* self.height, 'a')
#            energy = random.randrange(2 * self.sheep_gain_from_food)
            wage_a = random.normalvariate(initial_a_wise, initial_a_wise_sd)
            people = People(i,'a',(x, y), self, True, wage_a)
            self.grid.place_agent(people, (x, y))
            self.schedule.add(people)
            self.a_people.append(people)
        # Create b city people:
        for i in range(initial_b_people):
            x, y = self.position(self.height,self.width, 0.05* self.height, 'b')
#            energy = random.randrange(2 * self.sheep_gain_from_food)
            wage_b = random.normalvariate(initial_b_wise, initial_b_wise_sd)
            people = People(i,'b',(x, y), self, True, wage_b)
            self.grid.place_agent(people, (x, y))
            self.schedule.add(people)
            self.b_people.append(people)
        # Create a city firms
        for i in range(initial_a_firm):
            x, y = self.position(self.height,self.width, 0.05* self.height, 'a')
#            energy = random.randrange(2 * self.wolf_gain_from_food)
            wage_a = random.normalvariate(initial_a_wise, initial_a_wise_sd)
            firm = Firm(i,'a',(x, y), self, True, wage_a, init_a_margin_cost, init_a_fixed_cost)
            self.grid.place_agent(firm, (x, y))
            self.schedule.add(firm)
            self.a_firm.append(firm)
        # Create a city firms
        for i in range(initial_b_firm):
            x, y = self.position(self.height,self.width, 0.05* self.height, 'b')
#            energy = random.randrange(2 * self.wolf_gain_from_food)
            wage_b = random.normalvariate(initial_b_wise, initial_b_wise_sd)
            firm = Firm(i,'b',(x, y), self, True, wage_b, init_b_margin_cost, init_b_fixed_cost)
            self.grid.place_agent(firm, (x, y))
            self.schedule.add(firm)
            self.b_firm.append(firm)
        # Create grass patches
#        if self.grass:
#            for agent, x, y in self.grid.coord_iter():

#                fully_grown = random.choice([True, False])

#                if fully_grown:
#                    countdown = self.grass_regrowth_time
#                else:
#                    countdown = random.randrange(self.grass_regrowth_time)

#                patch = GrassPatch((x, y), self, fully_grown, countdown)
#                self.grid.place_agent(patch, (x, y))
#                self.schedule.add(patch)

        self.running = True

    def position(self, height, width, std, type):
        if type=='a':
            x = random.normalvariate(0.25* width, std)
            y = random.normalvariate(0.25* height, std)
            return int(x), int(y)
        elif type == 'b':
            x = random.normalvariate(0.75* width, std)
            y = random.normalvariate(0.75* height, std)
            return int(x), int(y)


    def step_people_wages(self):
        ## 计算城市人口数量
        self.a_people_num = self.count_people_num('a')
        self.b_people_num = self.count_people_num('b')
        if self.verbose:
            print(self.a_people_num, self.b_people_num)
        ## 计算城市人的名义工资
        self.a_average_nominate_wage = self.average_nominal_wages('a')
        self.b_average_nominate_wage = self.average_nominal_wages('b')
        ## 计算人口系数
        self.PeopleCoeffient = self.population_coefficient()
        ## 计算实际工资总和
        self.A_SumRealWages = self.sum_people_real_wage('a')
        self.B_SumRealWages = self.sum_people_real_wage('b')
        ## 计算预期工资
        self.A_SumExpectWages = self.sum_people_expect_wage('a')
        self.B_SumExpectWages = self.sum_people_expect_wage('b')
        ## 计算预期实际工资
        self.A_SumExpectRealWages = self.sum_people_expect_real_wage('a')
        self.B_SumExpectRealWages = self.sum_people_expect_real_wage('b')

    def step_firm_expect_profit(self):
        # 计算城市工厂数量
        self.a_firm_num = self.count_firm_num('a')
        self.b_firm_num = self.count_firm_num('b')
        # 计算产品实际价格综合
        self.A_SumRealPrice = self.sum_firm_product_price('a')
        self.B_SumRealPrice = self.sum_firm_product_price('b')
        # 计算本地价格指数
        self.A_LocalIndex = self.local_index(self.A_SumRealPrice, self.a_t_cost, self.sigma_a)
        self.B_LocalIndex = self.local_index(self.B_SumRealPrice, self.b_t_cost, self.sigma_b)
        ## 计算外地价格指数
        self.A_NonlocalIndex = self.nonlocal_index(self.B_SumRealPrice, self.a_t_cost, self.ab_t_cost, self.sigma_b)
        self.B_NonlocalIndex = self.nonlocal_index(self.A_SumRealPrice, self.b_t_cost, self.ab_t_cost, self.sigma_a)
        ## 计算价格指数
        self.A_Index = self.A_LocalIndex/self.A_NonlocalIndex
        self.B_Index = self.B_LocalIndex/self.B_NonlocalIndex
        ## 计算总需求
#        self.A_ProductNumber = self.product_number('a',self.a_t_cost,self.b_t_cost,self.ab_t_cost,self.A_SumRealPrice,self.B_SumRealPrice, self.sigma_a, self.sigma_b)
#        self.B_ProductNumber = self.product_number('b',self.a_t_cost,self.b_t_cost,self.ab_t_cost,self.A_SumRealPrice,self.B_SumRealPrice, self.sigma_a, self.sigma_b)
        ## 计算工厂收益
        self.A_Profit = self.profit('a',self.a_t_cost, self.b_t_cost, self.ab_t_cost, self.A_SumRealWages, self.B_SumRealWages, self.sigma_a, self.sigma_b)
        self.B_Profit = self.profit('b',self.a_t_cost, self.b_t_cost, self.ab_t_cost, self.A_SumRealWages, self.B_SumRealWages, self.sigma_a, self.sigma_b)
        ## 计算预期收益
        self.A_RespectProfit = self.expect_profit('a', self.a_t_cost, self.b_t_cost, self.ab_t_cost, self.A_SumRealWages, self.B_SumRealWages, self.sigma_a, self.sigma_b)
        self.B_RespectProfit = self.expect_profit('b', self.a_t_cost, self.b_t_cost, self.ab_t_cost, self.A_SumRealWages, self.B_SumRealWages, self.sigma_a, self.sigma_b)
        if self.verbose:
            print(self.A_Profit, self.B_Profit)


    def step_people_utility(self):
        ## 计算效用和预期效用
        self.utility('a')
        self.utility('b')
        ## 计算总效用
        self.A_TotalUtility = self.total_utility('a')
        self.B_TotalUtility = self.total_utility('b')
        ## 计算平均效用
        self.A_AverageUtility = self.A_TotalUtility/self.a_people_num
        self.B_AverageUtility = self.B_TotalUtility/self.b_people_num
        ## 计算平均收益

        self.A_AverageProfit = self.A_Profit/self.a_firm_num
        self.B_AverageProfit = self.B_Profit/self.b_firm_num

    def step_move(self):
        self.people_move()
        self.firm_move()


    def step(self):
        self.schedule.step()
        self.datacollector.collect(self)

        self.step_people_wages()
        self.step_firm_expect_profit()
        self.step_people_utility()
        self.step_move()
        if self.verbose:
            print([self.schedule.time])

#########################################################
    def run_model(self, step_count=200):

        if self.verbose:

            print(self.A_Profit)

        for i in range(step_count):
            self.step()

        if self.verbose:
            print('')

##########################################################
    def count(self, value):
        '''
        返回结果
        '''
        return value

##########################################################
    def count_people_num(self, types):
        '''
        计算人口数量
        :param type:城市
        :return:人口数量
        '''
        if types == 'a':
            return len(self.a_people)
        elif types == 'b':
            return len(self.b_people)

    def average_nominal_wages(self, types):
        '''
        计算平均名义工资
        :param type:城市
        :return: 平均名义工资
        '''
        SumWage = 0
        if types == 'a':
            for i in self.a_people:
                SumWage += i.Wage
            return SumWage / self.count_people_num('a')
        elif types == 'b':
            for i in self.b_people:
                SumWage += i.Wage
            return SumWage / self.count_people_num('b')

    def sum_people_real_wage(self,types):
        '''
        计算实际城市所有工人实际工资之和
        :param type: 城市
        :return: 实际工资和
        '''
        SumWage = 0
        if types == 'a':
            for a in self.a_people:
                a.real_wage(self.a_people_num, self.PeopleCoeffient)
                SumWage += a.RealWages
            return SumWage
        elif types == 'b':
            for b in self.b_people:
                b.real_wage(self.b_people_num, self.PeopleCoeffient)
                SumWage += b.RealWages
            return SumWage

    def sum_people_expect_wage(self,types):
        '''
        计算实际城市所有工人实际工资之和
        :param type: 城市
        :return: 实际工资和
        '''
        SumWage = 0
        if types == 'a':
            for a in self.a_people:
                a.expect_wage(self.a_wise, self.a_wise_sd)
                SumWage += a.ExpectWages
            return SumWage
        elif types == 'b':
            for b in self.b_people:
                b.expect_wage(self.b_wise, self.b_wise_sd)
                SumWage += b.ExpectWages
            return SumWage

    def sum_people_expect_real_wage(self,types):
        '''
        计算实际城市所有工人实际工资之和
        :param type: 城市
        :return: 实际工资和
        '''
        SumWage = 0
        if types == 'a':
            for a in self.a_people:
                a.expect_real_wage(self.a_people_num, self.PeopleCoeffient)
                SumWage += a.ExpectRealWages
            return SumWage
        elif types == 'b':
            for b in self.b_people:
                b.expect_real_wage(self.b_people_num, self.PeopleCoeffient)
                SumWage += b.ExpectRealWages
            return SumWage


    def population_coefficient(self):
        '''
        城市的人口系数
        :return: 城市人口系数
        '''
        return (self.count_people_num('a') + self.count_people_num('b'))/4.0

    def count_firm_num(self, types):
        '''
        计算工厂数量
        :param type:城市
        :return:工厂数量
        '''
        if types == 'a':
            return len(self.a_firm)
        elif types == 'b':
            return len(self.b_firm)

    def sum_firm_product_price(self,types):
        '''
        计算产品价格和
        :param type:城市
        :return:产品价格和
        '''
        SumProductPrice = 0
        if types == 'a':
            for a in self.a_firm:
                a.nominal_price(self.sigma_a)
                a.real_price(self.sigma_a)
                SumProductPrice += a.RealPrice
            return SumProductPrice
        elif types == 'b':
            for b in self.b_firm:
                b.nominal_price(self.sigma_b)
                b.real_price(self.sigma_b)
                SumProductPrice += b.RealPrice
            return SumProductPrice

    def local_index(self, cost, sum_price, sigma):
        '''
        计算本地物价指数
        :param cost: 成本
        :param sum_price: 实际价格和
        :param sigma:
        :return:本地物价指数
        '''
        return cost*sum_price**(1/(1-sigma))

    def nonlocal_index(self, cost, Tcost, sum_price, sigma):
        '''
        计算异地价格指数
        :param cost: 成本
        :param Tcost: 总成本
        :param sum_price: 实际价格和
        :param sigma:
        :return: 异地价格指数
        '''
        return cost * Tcost * sum_price**(1/(1-sigma))

    def index_city(self, cost, Tcost, sum_price, sigma):
        '''
        计算城市价格指数
        :param cost:
        :param Tcost:
        :param sum_price:
        :param sigma:
        :return: 城市价格指数
        '''
        return self.local_index(cost, sum_price, sigma)/self.nonlocal_index(cost,Tcost,sum_price,sigma)

    def product_number(self, types, cost1, cost2, Tcost, sum_price1, sum_price2, sigma1, sigma2):
        ProductNumber = 0
        if types == 'a':
            for firm in self.a_firm:
                ProductNumber += firm.productNumber(types,cost1,cost2,Tcost,sum_price1,sum_price2, sigma1, sigma2)
            return ProductNumber
        elif types =='b':
            for firm in self.b_firm:
                ProductNumber += firm.productNumber(types,cost1,cost2,Tcost,sum_price1,sum_price2, sigma1, sigma2)
            return ProductNumber

    def profit(self, types, cost1,cost2,Tcost,sum_price1,sum_price2,sigma1,sigma2):
        TProfit = 0
        if types == 'a':
            for firm in self.a_firm:
                a=firm.profit(types, cost1,cost2,Tcost,sum_price1,sum_price2,sigma1,sigma2)

                TProfit+=a
            return TProfit
        elif types == 'b':
            for firm in self.b_firm:
                a=firm.profit(types, cost2,cost1,Tcost,sum_price1,sum_price2,sigma1,sigma2)
                TProfit+=a

            return TProfit

    def expect_profit(self, types, cost1,cost2,Tcost,sum_price1,sum_price2,sigma1,sigma2):
        TExpectProfit = 0
        if types == 'a':
            for firm in self.a_firm:
                TExpectProfit+=firm.expect_profit(types,cost1,cost2,Tcost,sum_price1,sum_price2,sigma1,sigma2)
            return TExpectProfit
        elif types == 'b':
            for firm in self.b_firm:
                firm.expect_profit(types,cost1,cost2,Tcost,sum_price1,sum_price2,sigma1,sigma2)
            return TExpectProfit

    def utility(self,types):
        if types == 'a':
            for p in self.a_people:
                p.local_utility(types,self.sigma)
                p.nonlocal_utility(types,self.sigma)
                p.expect_local_utility(types,self.sigma)
                p.expect_nonlocal_utility(types,self.sigma)
                p.utility(types,self.sigma)
        elif types == 'b':
            for p in self.b_people:
                p.local_utility(types,self.sigma)
                p.nonlocal_utility(types,self.sigma)
                p.expect_local_utility(types,self.sigma)
                p.expect_nonlocal_utility(types,self.sigma)
                p.utility(type,self.sigma)

    def total_utility(self, types):
        SumUtility = 0
        if types =='a':
            for p in self.a_people:
                SumUtility += p.Utility
            return SumUtility
        elif types == 'b':
            for p in self.b_people:
                SumUtility += p.Utility
            return SumUtility

    def total_expect_utility(self,types):
        SumUtility = 0
        if types =='a':
            for p in self.a_people:
                SumUtility += p.ExpectUtility
            return SumUtility
        elif types == 'b':
            for p in self.b_people:
                SumUtility += p.ExpectUtility
            return SumUtility

    def people_move(self):
        for p in self.a_people:
            p.move()
        for p in self.b_people:
            p.move()

    def firm_move(self):
        for firm in self.a_firm:
            firm.move()
        for firm in self.b_firm:
            firm.move()
